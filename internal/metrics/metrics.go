package metrics

import (
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/yakshaving.art/shrrtnrr/internal/version"
)

var (
	namespace = "shrrtnrr"

	bootTime = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "boot_time_seconds",
		Help:      "unix timestamp of when the service was started",
	})

	buildInfo = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "build_info",
		Help:      "Build information",
	}, []string{"version", "commit", "date"})
)

// Exported metrics
var (
	ActiveURLs = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Subsystem: "urls",
		Name:      "active_total",
		Help:      "total number of URLs stored in memory",
	})

	RequestsDuration = promauto.NewHistogram(prometheus.HistogramOpts{
		Namespace: namespace,
		Subsystem: "requests",
		Name:      "duration_seconds",
		Help:      "Duration of requests",
		Buckets:   []float64{0.0001, 0.001, 0.01, 0.1, 0.5},
	})

	RequestsReceivedTotal = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: "requests",
			Name:      "received_total",
			Help:      "Total number of requests received",
		}, []string{"action"})

	ResponsesSent = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: "responses",
		Name:      "sent_total",
		Help:      "Responses sent out to clients",
	}, []string{"action", "return_code"})
)

func init() {
	bootTime.SetToCurrentTime()
	buildInfo.WithLabelValues(version.Version, version.Commit, version.Date).Set(1)
}

// Handler exposes the Prometheus metrics
func Handler() http.Handler {
	return promhttp.Handler()
}

// NewRequestTimer starts a timer for encode/decode operations
func NewRequestTimer() *Timer {
	return &Timer{
		Timer: prometheus.NewTimer(RequestsDuration),
	}
}

// Timer is a prometheus timer
type Timer struct {
	Timer *prometheus.Timer
}

// ObserveDuration observes the duration of a request
func (t Timer) ObserveDuration() time.Duration {
	return t.Timer.ObserveDuration()
}
