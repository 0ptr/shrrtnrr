# Shrrtnrr

One of those url shorteners, just a little different.

## The goal, because all projects need one, don't they?

There are two kinds of url shorteners out there:

1. Those that store in memory and wipe out everything at restart. Easy to
   deploy, not usable for long-lasting mappings.
2. Those that use persistence (db, file, stone tablets, whatever). Good for
   long-lasting mappings, harder to deploy.

What `shrrtnrr` does is making long-term mappings deployable with the same ease as
the in-memory shorteners without using a persistence layer. Keep reading.

### I'm listening

When it starts, `shrrtnrr` creates an empty in-memory map to be populated by the
users, just like url shorteners of type 1 (see above). Then it reads a static
mapping file and loads it in memory. This way the long-term mappings will be
preserved across restarts.

### The tradeoffs

* All urls that aren't in the static urls file will vanish when the process
  restarts.
* Shrrtnrr is intended to run as a single instance. Balancing requests across
  multiple instances will inevitably lead to inconsistent resolutions.

## Installation

The suggested way to run `shrrtnrr` is to build your own container using a git
repository (where you store the mapping file) and a CI pipeline, then deploy it.

## Usage

`shrrtnrr <args>`

### Arguments

All flags are optional.

#### -debug

Enable debug mode.

#### -homepage-url

Redirect the homepage to an external URL. Useful, for instance, to point to
corporate documentation about how `shrrtnrr` is implemented at a company.

#### -listen

The address:port to listen on. (default: "localhost:8765")

#### -scheme

The scheme to use in short urls: http or https. (default: "http")

This is particularly useful when using `shrrtnrr` behind a TLS terminator.

#### -static-urls

The file that contains the static urls.

## Sample static urls file

```yaml
urls:
- short: hurrdurr
  full: https://gitlab.com/yakshaving.art/hurrdurr
- short: wookie
  full: http://vignette1.wikia.nocookie.net/family-guy-the-quest-for-stuff/images/b/bd/Character-tricia-takanawa-facespace.png/revision/latest?cb=20140523115810
```
