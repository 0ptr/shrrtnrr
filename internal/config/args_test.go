package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yakshaving.art/shrrtnrr/internal/config"
)

func TestParseArgs(t *testing.T) {
	a := assert.New(t)
	tc := config.Args{
		Debug:          false,
		Listen:         "0.0.0.0:8765",
		Scheme:         "http",
		StaticURLsFile: "",
		QueryStringKey: "shrrtnrr_encode_url",
		Version:        false,
	}
	args := config.ParseArgs()
	t.Run("default arguments", func(t *testing.T) {
		a.Equal(args, tc)
	})
}

func TestCheck(t *testing.T) {
	tt := []struct {
		name   string
		args   config.Args
		err    string
		result config.Args
	}{
		{
			"default arguments",
			config.Args{
				Debug:          false,
				HomepageURL:    "",
				Listen:         "0.0.0.0:8765",
				Scheme:         "http",
				StaticURLsFile: "",
				QueryStringKey: "shrrtnrr_encode_url",
				Version:        false,
			},
			"",
			config.Args{
				Debug:          false,
				HomepageURL:    "",
				Listen:         "0.0.0.0:8765",
				Scheme:         "http",
				StaticURLsFile: "",
				QueryStringKey: "shrrtnrr_encode_url",
				Version:        false,
			},
		},
		{
			"invalid scheme",
			config.Args{
				Debug:          false,
				Listen:         "0.0.0.0:8765",
				Scheme:         "gopher",
				StaticURLsFile: "",
				QueryStringKey: "shrrtnrr_encode_url",
				Version:        false,
			},
			"invalid scheme gopher",
			config.Args{},
		},
		{
			"invalid listen address",
			config.Args{
				Listen: "static-urls.yaml",
			},
			"invalid listen address static-urls.yaml",
			config.Args{},
		},
	}

	for _, tc := range tt {
		a := assert.New(t)
		t.Run(tc.name, func(t *testing.T) {
			err := tc.args.Check()
			if tc.err != "" {
				a.EqualError(err, tc.err)
			} else {
				a.NoError(err)
				a.Equal(tc.result, tc.args)
			}
		})
	}
}
