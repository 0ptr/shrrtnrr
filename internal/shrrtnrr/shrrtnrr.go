package shrrtnrr

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/teris-io/shortid"
	"gitlab.com/yakshaving.art/shrrtnrr/internal/config"
	"gitlab.com/yakshaving.art/shrrtnrr/internal/metrics"
)

// Shrrtnrr is a URL shortener
type Shrrtnrr struct {
	HomepageURL    string
	QueryStringKey string
	r              *http.Request
	requestTimer   *metrics.Timer
	EncodeTemplate *template.Template
	Scheme         string
	URLs           map[string]string
	w              http.ResponseWriter
}

type encodeTemplate struct {
	ShortURL string
}

// New creates a new shrrtnrr
func New(args config.Args) *Shrrtnrr {
	URLs, err := config.LoadStaticURLs(args.StaticURLsFile)
	if err != nil {
		log.Fatalf("unable to read the static urls file: %s", err)
	}
	metrics.ActiveURLs.Set(float64(len(URLs)))
	return &Shrrtnrr{
		HomepageURL:    args.HomepageURL,
		QueryStringKey: args.QueryStringKey,
		EncodeTemplate: config.LoadTemplate(),
		Scheme:         args.Scheme,
		URLs:           URLs,
	}
}

// Handler is the default handler for the http server
func (s Shrrtnrr) Handler() http.Handler {
	handleFunc := func(w http.ResponseWriter, r *http.Request) {
		s.requestTimer = metrics.NewRequestTimer()
		s.r = r
		s.w = w

		// Encode if it's got a url key in the query string
		url, ok := r.URL.Query()[s.QueryStringKey]
		if ok {
			metrics.RequestsReceivedTotal.WithLabelValues("encode").Inc()
			s.EncodeURL(url[0])
			return
		}

		// Decode if the url begins with /d/
		if strings.HasPrefix(r.URL.String(), "/d/") {
			metrics.RequestsReceivedTotal.WithLabelValues("encode").Inc()
			s.DecodeURL(r.URL.String()[3:])
			return
		}

		// Homepage
		metrics.RequestsReceivedTotal.WithLabelValues("default").Inc()
		if s.HomepageURL != "" {
			http.Redirect(s.w, s.r, s.HomepageURL, http.StatusFound)
			return
		}
		w.WriteHeader(http.StatusNotImplemented)
		s.requestTimer.ObserveDuration()
		return
	}
	return http.HandlerFunc(handleFunc)
}

// EncodeURL encodes a long URL into a hash and returns a short URL
func (s Shrrtnrr) EncodeURL(url string) {
	if url == "" {
		s.requestTimer.ObserveDuration()
		metrics.ResponsesSent.WithLabelValues("encode", strconv.Itoa(http.StatusBadRequest)).Inc()
		s.w.WriteHeader(http.StatusBadRequest)
		s.w.Write([]byte("URL not provided\n"))
		return
	}
	log.Debugf("encoding %s", url)
	hash := shortid.MustGenerate()
	log.Debugf("url: %s - hash: %s", url, hash)
	s.URLs[hash] = url
	metrics.ActiveURLs.Inc()
	renderedURL := fmt.Sprintf("%s://%s/d/%s", s.Scheme, s.r.Host, hash)
	err := s.EncodeTemplate.Execute(s.w, encodeTemplate{ShortURL: renderedURL})
	if err != nil {
		log.Error(err)
		return
	}
	s.requestTimer.ObserveDuration()
	metrics.ResponsesSent.WithLabelValues("encode", strconv.Itoa(http.StatusOK)).Inc()
	return
}

// DecodeURL decodes a hash into a URL
func (s Shrrtnrr) DecodeURL(hash string) {
	log.Debugf("decoding %s", hash)
	url := s.URLs[hash]
	log.Debugf("retrieved url: %s", url)
	if url == "" {
		s.requestTimer.ObserveDuration()
		metrics.ResponsesSent.WithLabelValues("decode", strconv.Itoa(http.StatusNotFound)).Inc()
		s.w.WriteHeader(http.StatusNotFound)
		s.w.Write([]byte("URL Not Found\n"))
		return
	}
	s.requestTimer.ObserveDuration()
	metrics.ResponsesSent.WithLabelValues("decode", strconv.Itoa(http.StatusMovedPermanently)).Inc()
	http.Redirect(s.w, s.r, url, http.StatusMovedPermanently)
}
