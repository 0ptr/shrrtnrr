package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yakshaving.art/shrrtnrr/internal/config"
)

func TestLoadingConfig(t *testing.T) {
	tt := []struct {
		name     string
		filename string
		err      string
		result   map[string]string
	}{
		{
			name:     "empty filename works",
			filename: "",
			err:      "",
			result:   map[string]string{},
		},
		{
			name:     "empty config works",
			filename: "test-fixtures/empty-config.yaml",
			err:      "",
			result:   map[string]string{},
		},
		{
			name:     "valid config works",
			filename: "test-fixtures/valid-config.yaml",
			err:      "",
			result: map[string]string{
				"this-is-a-test-short-url": "https://sample.url/that/happens?to=be&very=long",
				"this-is-another-test":     "https://another.url.that/contains/more#/random?_g=()&_a=(craziness)",
			},
		},
		{
			name:     "non-existing file fails",
			filename: "test-fixtures/non-existing-config.yaml",
			err:      "failed to read static urls file test-fixtures/non-existing-config.yaml: open test-fixtures/non-existing-config.yaml: no such file or directory",
		},
		{
			name:     "invalid-config fails",
			filename: "test-fixtures/invalid-config.yaml",
			err:      "failed to parse static urls file test-fixtures/invalid-config.yaml: yaml: line 5: could not find expected ':'",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			a := assert.New(t)

			c, err := config.LoadStaticURLs(tc.filename)
			if tc.err != "" {
				a.EqualError(err, tc.err)
			} else {
				a.NoError(err)
				a.Equal(tc.result, c)
			}
		})
	}
}
