package main

import (
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/yakshaving.art/shrrtnrr/internal/config"
	"gitlab.com/yakshaving.art/shrrtnrr/internal/metrics"
	"gitlab.com/yakshaving.art/shrrtnrr/internal/shrrtnrr"
)

func main() {
	args := config.ParseArgs()
	if err := args.Check(); err != nil {
		log.Fatal(err)
	}

	log.SetLevel(log.InfoLevel)
	if args.Debug {
		log.SetLevel(log.DebugLevel)
	}

	s := shrrtnrr.New(args)
	http.Handle("/emil", http.RedirectHandler("https://string-emil.de", 301))
	http.Handle("/metrics", metrics.Handler())
	http.Handle("/", s.Handler())
	// TODO: Add context with timeout

	log.Info("Listening on ", args.Listen)
	log.Fatal(http.ListenAndServe(args.Listen, nil))
}
