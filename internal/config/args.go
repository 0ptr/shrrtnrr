package config

import (
	"flag"
	"fmt"

	validator "github.com/go-playground/validator/v10"
)

// Args are the arguments that can be used in this binary.
type Args struct {
	Debug          bool
	HomepageURL    string
	Scheme         string
	Listen         string
	StaticURLsFile string
	QueryStringKey string
	Version        bool
}

// ParseArgs parses the ccommand line arguments
func ParseArgs() Args {
	args := Args{}

	flag.BoolVar(&args.Debug, "debug", false, "enable debug level logging")
	flag.BoolVar(&args.Version, "version", false, "show version and exit")
	flag.StringVar(&args.Listen, "listen", "0.0.0.0:8765", "address:port where to listen on (default: localhost:8765)")
	flag.StringVar(&args.QueryStringKey, "query-string-key", "shrrtnrr_encode_url", "query string key to read long URLs")
	flag.StringVar(&args.Scheme, "scheme", "http", "the scheme to use when returning short urls: http or https (default: http)")
	flag.StringVar(&args.StaticURLsFile, "static-urls", "", "file containing the static urls")
	flag.StringVar(&args.HomepageURL, "homepage-url", "", "URL to load instead of the builtin homepage")

	flag.Parse()
	return args

}

// Check validates the arguments
func (a Args) Check() error {
	v := validator.New()
	err := v.Var(a.Listen, "hostname_port")
	if err != nil {
		return fmt.Errorf("invalid listen address %s", a.Listen)
	}

	if !(a.Scheme == "http" || a.Scheme == "https") {
		return fmt.Errorf("invalid scheme %s", a.Scheme)
	}

	return nil
}
