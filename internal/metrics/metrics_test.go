package metrics_test

import (
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/yakshaving.art/shrrtnrr/internal/metrics"
)

func TestMetricsAreRegistered(t *testing.T) {
	a := assert.New(t)
	a.True(prometheus.DefaultRegisterer.Unregister(metrics.ActiveURLs),
		"webhooks active URLs")
	a.True(prometheus.DefaultRegisterer.Unregister(metrics.RequestsDuration),
		"invalid requests duration")
	a.True(prometheus.DefaultRegisterer.Unregister(metrics.RequestsReceivedTotal),
		"invalid requests received")
	a.True(prometheus.DefaultRegisterer.Unregister(metrics.ResponsesSent),
		"invalid responses sent")

}
