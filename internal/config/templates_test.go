package config

import (
	"fmt"
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

type encodeTemplate struct {
	ShortURL string
}

func TestLoadTemplate(t *testing.T) {
	tt := []struct {
		name     string
		want     string
		contains func(assert.TestingT, interface{}, interface{}, ...interface{}) bool
	}{
		{
			"valid template",
			"valid-response-template.html",
			assert.Contains,
		},
		{
			"empty link",
			"empty-link-template.html",
			assert.NotContains,
		},
		{
			"invalid link",
			"invalid-link-template.html",
			assert.NotContains,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			tmpl := LoadTemplate()
			tmpl.Execute(ioutil.Discard, encodeTemplate{ShortURL: "this-is-a-test-string"})
			want, err := ioutil.ReadFile(fmt.Sprintf("test-fixtures/%s", tc.want))
			if err != nil {
				t.Fatalf("unable to open test fixture %s: %s", tc.want, err)
			}

			tc.contains(t, string(want), `<h3 id="shortlink">this-is-a-test-string</h3>`)
		})
	}
}
