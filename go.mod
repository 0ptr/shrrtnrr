module gitlab.com/yakshaving.art/shrrtnrr

go 1.14

require (
	github.com/go-playground/validator/v10 v10.2.0
	github.com/prometheus/client_golang v1.5.1
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/testify v1.5.1
	github.com/teris-io/shortid v0.0.0-20171029131806-771a37caa5cf
	gopkg.in/yaml.v2 v2.2.8
)
