package shrrtnrr_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yakshaving.art/shrrtnrr/internal/config"
	"gitlab.com/yakshaving.art/shrrtnrr/internal/shrrtnrr"
)

func TestNew(t *testing.T) {
	tt := []struct {
		name string
		args config.Args
		want *shrrtnrr.Shrrtnrr
	}{
		{
			"default config",
			config.Args{
				StaticURLsFile: "",
				QueryStringKey: "shrrtnrr_encode_url",
			},
			&shrrtnrr.Shrrtnrr{
				URLs:           map[string]string{},
				QueryStringKey: "shrrtnrr_encode_url",
			},
		},
		{
			"with different query string key",
			config.Args{
				StaticURLsFile: "",
				QueryStringKey: "something_else",
			},
			&shrrtnrr.Shrrtnrr{
				URLs:           map[string]string{},
				QueryStringKey: "something_else",
			},
		},
		{
			"with valid static urls file",
			config.Args{
				StaticURLsFile: "../config/test-fixtures/valid-config.yaml",
				QueryStringKey: "shrrtnrr_encode_url",
			},
			&shrrtnrr.Shrrtnrr{
				URLs: map[string]string{
					"this-is-a-test-short-url": "https://sample.url/that/happens?to=be&very=long",
					"this-is-another-test":     "https://another.url.that/contains/more#/random?_g=()&_a=(craziness)",
				},
				QueryStringKey: "shrrtnrr_encode_url",
			},
		},
	}
	for _, tc := range tt {
		a := assert.New(t)
		t.Run(tc.name, func(t *testing.T) {
			got := shrrtnrr.New(tc.args)
			a.Equal(got.URLs, tc.want.URLs)
			a.Equal(got.QueryStringKey, tc.want.QueryStringKey)
			a.Equal(got.Scheme, tc.want.Scheme)
		})
	}
}

func TestShrrtnrr_Handler(t *testing.T) {
	tt := []struct {
		name         string
		shrrtnrr     shrrtnrr.Shrrtnrr
		url          string
		expectedCode int
	}{
		{
			"default behaviour",
			shrrtnrr.Shrrtnrr{},
			"/",
			http.StatusNotImplemented,
		},
		{
			"with an alternative homepage url",
			shrrtnrr.Shrrtnrr{
				HomepageURL: "https://google.com",
			},
			"/",
			http.StatusFound,
		},
	}

	for _, tc := range tt {
		a := assert.New(t)
		s := tc.shrrtnrr
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", tc.url, nil)
			if err != nil {
				t.Fatal(err)
			}
			rr := httptest.NewRecorder()
			handler := http.Handler(s.Handler())
			handler.ServeHTTP(rr, req)
			a.Equal(rr.Code, tc.expectedCode)
		})
	}
}

func TestShrrtnrr_EncodeURL(t *testing.T) {
	tt := []struct {
		name         string
		url          string
		expectedCode int
	}{
		{
			"with a valid url",
			"https://sample.url/that/happens?to=be&very=long",
			http.StatusOK,
		},
		{
			"with an empty url",
			"",
			http.StatusBadRequest,
		},
	}

	for _, tc := range tt {
		a := assert.New(t)
		s := shrrtnrr.Shrrtnrr{
			EncodeTemplate: config.LoadTemplate(),
			QueryStringKey: "shrrtnrr_encode_url",
			URLs:           map[string]string{},
		}
		t.Run(tc.name, func(t *testing.T) {
			r := fmt.Sprintf("/?%s=%s", s.QueryStringKey, tc.url)
			req, err := http.NewRequest("GET", r, nil)
			if err != nil {
				t.Fatal(err)
			}
			rr := httptest.NewRecorder()
			handler := http.Handler(s.Handler())
			handler.ServeHTTP(rr, req)
			a.Equal(rr.Code, tc.expectedCode)
		})
	}
}

func TestShrrtnrr_DecodeURL(t *testing.T) {
	tt := []struct {
		name         string
		s            shrrtnrr.Shrrtnrr
		hash         string
		expectedCode int
		expectedURL  string
	}{
		{
			"with a valid hash",
			shrrtnrr.Shrrtnrr{
				URLs: map[string]string{
					"this-is-a-test-short-url": "https://sample.url/that/happens?to=be&very=long",
				},
			},
			"this-is-a-test-short-url",
			http.StatusMovedPermanently,
			"https://sample.url/that/happens?to=be&very=long",
		},
		{
			"without any hashes",
			shrrtnrr.Shrrtnrr{
				URLs: map[string]string{},
			},
			"this-is-a-test-short-url",
			http.StatusNotFound,
			"",
		},
		{
			"with a non-existing hash",
			shrrtnrr.Shrrtnrr{
				URLs: map[string]string{},
			},
			"this-is-a-test-short-url",
			http.StatusNotFound,
			"",
		},
	}

	for _, tc := range tt {
		a := assert.New(t)
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", "/d/"+tc.hash, nil)
			if err != nil {
				t.Fatal(err)
			}

			rr := httptest.NewRecorder()
			handler := http.Handler(tc.s.Handler())
			handler.ServeHTTP(rr, req)

			if status := rr.Code; status != tc.expectedCode {
				t.Errorf("handler returned wrong status code: got %v want %v",
					status, tc.expectedCode)
			}

			if tc.expectedCode == http.StatusNotFound {
				a.EqualValues(0, len(rr.Header()["Location"]))
			} else {
				a.Equal(tc.expectedURL, rr.Header()["Location"][0])
			}
		})
	}

}
